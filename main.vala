// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>
// SPDX-License-Identifier: GPL-3.0-only

private void print_option(string key, GLib.Variant val) {
  print("got option [%s: %s]", key, val.print(false));
}

private void on_signal(string id, GLib.HashTable<string, GLib.Variant> options) {
  print("got signal for id %s\n", id);
  options.for_each(print_option);
}

public int main(string[] args) {
  try {
    var test = GLib.Bus.get_proxy_sync<org.test.TestNs.TestInterface>(SESSION,
                                                                      "org.test.Test",
                                                                      "/org/test/Test");
    var options = new GLib.HashTable<string, GLib.Variant>(GLib.str_hash, GLib.str_equal);
    test.test_method.begin(options);
    test.test_signal.connect(on_signal);
    if (test.test_property != null)
      test.test_property.for_each(print_option);
  } catch (GLib.Error err) {
    error("%s", err.message);
  }
  return 0;
}
